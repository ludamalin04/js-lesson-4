"use strict"
// /**
//  * Завдання 1.
//  *
//  * Користувач вводить 3 числа.
//  * Вивести в консоль повідомлення з максимальним числом із введених.
//  *
//  * Якщо одне із введених користувачем чисел не є числом,
//  * вивести повідомлення: «⛔️ Помилка! Одне з введених чисел не є числом..
//  *
//  * Умови: об'єктом Math користуватися не можна.
//  */
let num1 = prompt("Enter first number");
let num2 = prompt("Enter second number");
let num3 = prompt("Enter third number");

function isValidNumber(number) {
    if (isNaN(number) || number === null || number === "") {
        return null;
    } else {
        return Number(number);
    }
}
if(isValidNumber(num1) && isValidNumber(num2) && isValidNumber(num3)){
    Math.max(num1, num2, num3);
    alert(Math.max(num1, num2, num3));
} else alert("⛔️ Помилка! Одне з введених чисел не є числом");

console.log(isValidNumber(num1));
console.log(isValidNumber(num2));
console.log(isValidNumber(num3));
